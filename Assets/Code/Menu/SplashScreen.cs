﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour 
{
	#region Fields
	Color textColor = Color.white;
	[SerializeField]
	Texture2D splashImage;
	#endregion
	
	void Start () 
	{

	}

	void Update () 
	{
		
	}
	
	void OnGUI()
	{
		GUI.color = Color.white;
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), splashImage);

		GUI.color = textColor;
		if(GUI.Button (new Rect(0, Screen.height * 0.9f, Screen.width, Screen.height * 0.1f), "PRESS START"))
		{
			StartCoroutine(ShowMenu());
		}
	}
	
	IEnumerator ShowMenu()
	{
		for(float i = 0 ; i < 1 ; i += 0.01f)
		{
			yield return new WaitForSeconds(0.01f);
			textColor = new Color(1,1,1, 1 - i);
		}

		Application.LoadLevel("MainMenu");
	}
}
