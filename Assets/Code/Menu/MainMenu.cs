﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour 
{
	public Texture2D wallpaper;

	public float buttonWidth;
	public float buttonHeight;
	public float buttonGap;

	public GUISkin skin;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnGUI()
	{
		float offset = 0.25f;
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), wallpaper);

		GUI.skin = skin;
		if(GUI.Button (new Rect(Screen.width * offset, Screen.height * 0.4f, buttonWidth, buttonHeight), "Level 1"))
		{
			Application.LoadLevel("Level 1");
		}

		if(GUI.Button (new Rect(Screen.width * offset + buttonGap * 0.5f, Screen.height * 0.4f + (buttonGap + buttonHeight), buttonWidth, buttonHeight), "Level 2"))
		{
			Application.LoadLevel("Level 2");
		}

		if(GUI.Button (new Rect(Screen.width * offset + buttonGap * 0.5f, Screen.height * 0.4f + 2 * (buttonGap + buttonHeight), buttonWidth, buttonHeight), "Level 3"))
		{
			Application.LoadLevel("Level 3");
		}

		if(GUI.Button (new Rect(Screen.width * offset + buttonGap , Screen.height * 0.4f + 3 * (buttonGap + buttonHeight), buttonWidth, buttonHeight), "Exit"))
		{
			Application.Quit();
		}
	}
}
