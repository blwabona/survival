﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour 
{

	#region Singleton
	private static SoundController instance;
	public static SoundController Instance
	{
		get
		{
			return (instance ? instance : new GameObject("Singleton - SoundController").AddComponent<SoundController>());
		}
	}
	#endregion 

	public AudioClip shootFX;
	public AudioClip hearthBeatFX;
	public AudioClip gameOverFX;
	public AudioClip menuClickFX;
	public AudioClip menuMusic;
	public AudioClip gameplayMusic;

	public AudioClip levelUpFX;
	public AudioClip upgradeFX;


	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
