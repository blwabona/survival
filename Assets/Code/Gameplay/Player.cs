﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
	#region Fields

	// COMPONENT REFERENCES
	public Stats stats;
	public Weapon weapon;

	// FIELDS
	public Texture2D whitePixel;
	public float healthBarWidth;
	public float healthBarHeight;
	public float healthBarYPos;
	public float delimiterSize;

	public Color healthColor;
	public Color staminaColor;
	#endregion


	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnGUI()
	{
		if(stats.health > 0)
		{
			Screen.lockCursor = true;

			// Static Content
			GUI.color = Color.black;
			GUI.DrawTexture (new Rect(Screen.width / 2 - healthBarWidth - delimiterSize, healthBarYPos, healthBarWidth, healthBarHeight), whitePixel);
			GUI.DrawTexture (new Rect(Screen.width / 2, healthBarYPos + healthBarHeight/4, healthBarWidth, healthBarHeight/2), whitePixel);

			GUI.color = Color.white;
			GUI.DrawTexture (new Rect(Screen.width / 2 - delimiterSize, healthBarYPos - 2 * delimiterSize, delimiterSize, healthBarHeight + 4 * delimiterSize), whitePixel);

			// Health Bar
			GUI.color = healthColor;
			GUI.DrawTexture(new Rect(Screen.width / 2 - healthBarWidth * stats.healthRatio - delimiterSize, healthBarYPos, healthBarWidth * stats.healthRatio, healthBarHeight), whitePixel);

			// Stamina Bar
			GUI.color = staminaColor;
			GUI.DrawTexture (new Rect(Screen.width / 2, healthBarYPos + healthBarHeight/4, healthBarWidth * stats.staminaRatio, healthBarHeight/2), whitePixel);
		}
		else
		{
			Screen.lockCursor = false;

			GUI.color = Color.black;
			GUI.DrawTexture (new Rect(0,0, Screen.width, Screen.height), whitePixel);

			GUI.color = Color.white;
			if(GUI.Button (new Rect(Screen.width * 0.1f, Screen.height * 0.9f , Screen.width * 0.3f, Screen.height * 0.1f), "RETRY"))
			{
				Application.LoadLevel (Application.loadedLevelName);
			}
		   
			if(GUI.Button (new Rect(Screen.width * 0.6f, Screen.height * 0.9f , Screen.width * 0.3f, Screen.height * 0.1f), "EXIT"))
			{
				Application.LoadLevel ("MainMenu");
			}
		}
	}
}
