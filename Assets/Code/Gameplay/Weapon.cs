﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour 
{
	#region Fields
	public int clipSize;
	public int ammoSize;
	public int maxAmmo;

	public int damage;
	public float shootCooldown;
	public int reloadDuration;
	public bool canShoot;
	public Stats playerStats;
	public bool isEquipped;

	public Texture2D crossHair;
	public Vector2 crossHairPos;
	public float crossHairSize;

	public Vector2 hudAmmoPos;
	public float hudAmmoWidth;
	public float hudAmmoHeight;

	public GUISkin largeSkin;
	public GUISkin defaultSkin;
	#endregion
	
	void Start () 
	{
		crossHairPos = new Vector2(Screen.width/2, Screen.height/2);
		hudAmmoPos = new Vector2(40, 40);
	}

	void Update () 
	{
		if(Input.GetMouseButtonDown(0))
		{
			this.Shoot ();
		}
	}

	void OnGUI()
	{
		GUI.skin = largeSkin;
		GUI.Label (new Rect(hudAmmoPos.x, hudAmmoPos.y, hudAmmoWidth, hudAmmoHeight), "AMMO:  " + this.clipSize + "\nSKILL pts: " + this.playerStats.skillPoints);

		GUI.skin = defaultSkin;
		GUI.DrawTexture(new Rect(crossHairPos.x - crossHairSize/2, crossHairPos.y - crossHairSize/2, crossHairSize, crossHairSize), crossHair);
	}

	void Shoot()
	{

		if(this.clipSize > 0)
		{
			// SHOOT
			if(this.canShoot)
			{
				this.clipSize--;
				gameObject.audio.Play();

				Ray ray = Camera.main.ScreenPointToRay( crossHairPos );
				
				// the raycast hit info will be filled by the Physics.Raycast() call further
				RaycastHit hit;

				if( Physics.Raycast( ray, out hit ) )
				{
					if( hit.collider.gameObject.tag == "Enemy" )
					{
						Enemy enemy = (Enemy)hit.collider.gameObject.GetComponent(typeof(Enemy));
						Debug.Log ("Player Strength: "+playerStats.strength);
						enemy.DecreaseHealth(this.damage + playerStats.strength, playerStats);
					}
				}
			}
		}
		else if(this.maxAmmo > 0)
		{
			// RELOAD
		}
		else
		{
			// NOTIFY USER ABOUT RELOADING AMMO
		}
	}

	IEnumerator ShootingCooldown()
	{
		this.canShoot = false;
		yield return new WaitForSeconds(shootCooldown);
		this.canShoot = true;

	}
}
