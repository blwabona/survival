﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour 
{
	#region Fields

	[SerializeField]
	GameObject basicEnemy;

	[SerializeField]
	GameObject player;

	Vector3 position;
	Vector3 sinPos;

	[SerializeField]
	float spawnDistance;
	
	private float counter;

	[SerializeField]
	private float pulseSpeed;

	[SerializeField]
	private bool isVisible;

	public int EnemyLimit;

	#endregion Fields

	#region properties

	int EnemyCount
	{
		get
		{
			return GameObject.FindGameObjectsWithTag("Enemy").Length;
		}
	}
	#endregion

	#region Functions
	// Use this for initialization
	void Start () 
	{
		EnemyLimit = 10;
		InvokeRepeating ("SpawnEnemy", 2, 5);
		this.position = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//StartCoroutine(SpawnEnemy());
		counter += pulseSpeed * Time.deltaTime;
		if(counter > 360)
		{
			counter = 0;
		}
		float amplitude = 0.3f;
		this.sinPos = new Vector3(this.position.x, this.position.y + amplitude * Mathf.Sin(counter), this.position.z);
		this.transform.position = this.sinPos;
	}

	void OnBecameVisible()
	{
		Debug.Log("VISIBLE");
		this.isVisible = true;
	}
	
	void OnBecameInvisible()
	{
		Debug.Log("INVISIBLE");
		this.isVisible = false;
	}

	void SpawnEnemy()
	{
		if(true)//!this.isVisible)
		{
			if(EnemyCount < EnemyLimit)
			{
				if((this.transform.position - player.transform.position).magnitude > spawnDistance)
				{
					GameObject enemyObject = (GameObject)Instantiate(basicEnemy, this.position, transform.rotation);
					Stats enemyStats = (Stats)enemyObject.GetComponent(typeof(Stats));
					enemyStats.speed = 3.5f;
					Debug.Log("Enemy Spawned - Count: "+EnemyCount);
				}
			}
		}
	}
	#endregion 
}
