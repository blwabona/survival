﻿using UnityEngine;
using System.Collections;

public class Shrine : MonoBehaviour 
{
	public bool isActive;
	[SerializeField]
	private Texture2D symbol;

	[SerializeField]
	private Texture2D activeSymbol;

	[SerializeField]
	private float iconSize;

	private float counter;
	Player player
	{
		get
		{
			return (FindObjectOfType(typeof(Player)) as Player);//(Player)(GameObject.FindGameObjectWithTag("Player")).GetComponent(typeof(Player));
		}
	}

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		// Spin Cube
		counter += Time.deltaTime;
		transform.Rotate(Mathf.Sin (counter), Mathf.Cos (counter), Mathf.Sin (counter), Space.Self);
	}

	void OnGUI()
	{
		if(this.isActive)
		{
			if(player)
			{
				if(player.stats.skillPoints > 0)
				{
					GUI.DrawTexture(new Rect((Screen.width - iconSize)/2, (Screen.height - iconSize)/2, iconSize, iconSize), activeSymbol);
				}
				else
				{
					GUI.DrawTexture(new Rect((Screen.width - iconSize)/2, (Screen.height - iconSize)/2, iconSize, iconSize), symbol);
				}
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		GameObject enteringObject = other.gameObject;
		if(enteringObject.tag == "Player")
		{
			this.Activate();
		}
	}

	void OnTriggerExit(Collider other)
	{
		GameObject enteringObject = other.gameObject;
		if(enteringObject.tag == "Player")
		{
			this.Deactivate();
		}
	}

	void Activate()
	{
		this.isActive = true;
	}

	void Deactivate()
	{
		this.isActive = false;
	}
}
