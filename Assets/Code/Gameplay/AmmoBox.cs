﻿using UnityEngine;
using System.Collections;

public class AmmoBox : MonoBehaviour 
{
	bool ammoAvailable;

	[SerializeField]
	private float cooldownDuration;

	private float counter;

	[SerializeField]
	private int reloadAmount;

	Player player
	{
		get
		{
			return (Player)FindObjectOfType(typeof(Player));
		}
	}

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(this.ammoAvailable)
		{
			counter += Time.deltaTime;
			transform.Rotate(Mathf.Sin (counter), Mathf.Cos (counter), Mathf.Sin (counter), Space.Self);
		}
	}

	void OnTriggerEnter(Collider other)
	{
		GameObject enteringObject = other.gameObject;
		if(enteringObject.tag == "Player" && this.ammoAvailable)
		{
			this.ammoAvailable = false;
			player.weapon.clipSize += reloadAmount;
			this.audio.Play ();
		}
	}
	
	void OnTriggerExit(Collider other)
	{
		GameObject exitingObject = other.gameObject;
		if(exitingObject.tag == "Player")
		{
			StartCoroutine(AmmoCooldown());
		}
	}

	IEnumerator AmmoCooldown()
	{
		yield return new WaitForSeconds(cooldownDuration);
		this.ammoAvailable = true;
	}
}
