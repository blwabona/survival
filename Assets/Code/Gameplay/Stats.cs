﻿using UnityEngine;
using System.Collections;

public class Stats : MonoBehaviour 
{
	public float health;
	public float maxHealth;
	public float healthRegenRate;
	public float healthRatio;
	
	public float stamina;
	public float maxStamina;
	public float staminaRegenRate;
	public float staminaRatio;

	public float strength;
	public float speed;
	public float jumpHeight;

	public float XP;
	public float upgradeXP;
	public int skillPoints;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		healthRatio = health / maxHealth;
		staminaRatio = stamina / maxStamina;

		this.RegenerateHealth();
		this.RegenerateStamina();
	}

	public void RegenerateHealth()
	{
		if(health > 0)
		{
			this.AdjustHealth(this.healthRegenRate * Time.deltaTime);
		}
	}
	
	public void RegenerateStamina()
	{
		this.AdjustStamina(this.staminaRegenRate * Time.deltaTime);
	}


	public void UpgradeHealth()
	{
		this.maxHealth += 10;
		this.health = this.maxHealth;
	}

	public void UpgradeStrength()
	{
		this.strength += 10;
	}

	public void UpgradeAgility()
	{
		this.maxStamina += 10;
		this.stamina = this.maxStamina;
	}

	public void AdjustHealth(float deltaHealth)
	{
		this.health += deltaHealth;
		if(this.health > this.maxHealth)
		{
			this.health = this.maxHealth;
		}
		else if(this.health <= 0)
		{
			// Player Dead
			this.health = 0;
		}
	}

	public void AdjustStamina(float deltaStamina)
	{
		this.stamina += deltaStamina;
		if(this.stamina > this.maxStamina)
		{
			this.stamina = this.maxStamina;
		}
		else if(this.stamina <= 0)
		{
			this.stamina = 0;
		}
	}

	public void IncreaseXP(float deltaXP)
	{
		this.XP += deltaXP;
		if(this.XP >= upgradeXP)
		{
			this.LevelUp ();
		}
	}



	public void LevelUp()
	{
		this.skillPoints++;
	}
}
