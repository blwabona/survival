﻿using UnityEngine;
using System.Collections;

public class ChaseAI : MonoBehaviour 
{
	public Transform player;

	[SerializeField]
	float MinDist;

	[SerializeField]
	float MaxDist;

	[SerializeField]
	Stats EnemyStats;

	bool canAttackPlayer;

	void Start()
	{

	}

	void Update () 
	{
		ChasePlayer();
	}

	void ChasePlayer()
	{
		transform.LookAt(player);
		
		if(Vector3.Distance(transform.position,player.position) >= MinDist)
		{
			transform.position += transform.forward* EnemyStats.speed*Time.deltaTime;

			if(Vector3.Distance(transform.position,player.position) <= MinDist)
			{

				StartCoroutine(AttackPlayer());
			}			
		}
	}

	IEnumerator AttackPlayer()
	{
		if(canAttackPlayer)
		{
			if(Vector3.Distance(transform.position,player.position) <= MinDist)
			{
				Stats playerStats = player.gameObject.GetComponentInChildren(typeof(Stats)) as Stats;
				playerStats.AdjustHealth(-EnemyStats.strength);
				canAttackPlayer = false;
			}
		}

		yield return new WaitForSeconds(1f);
		canAttackPlayer = true;
	}
}
