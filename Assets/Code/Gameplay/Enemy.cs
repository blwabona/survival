﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour 
{

	public Stats stats;
	public ChaseAI chaseAI;
	public GameObject head;

	[SerializeField]
	private Texture2D block;

	[SerializeField]
	private float barWidth;

	[SerializeField]
	private float barHeight;

	public bool isVisible;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	void OnBecameVisible()
	{
		Debug.Log("VISIBLE");
		this.isVisible = true;
	}

	void OnBecameInvisible()
	{
		Debug.Log("INVISIBLE");
		this.isVisible = false;
	}

	void OnGUI()
	{
		if((chaseAI.player.position - transform.position).magnitude < 10 && this.isVisible)
		{
			Vector3 position = Camera.main.WorldToScreenPoint(head.transform.position);

			GUI.color = Color.black;
			GUI.DrawTexture(new Rect(position.x - barWidth/2, (Screen.height - position.y) - 20,  barWidth, barHeight), block);

			GUI.color = Color.red;
			GUI.DrawTexture(new Rect(position.x - barWidth/2 + 2, (Screen.height - position.y) - 18, barWidth * stats.healthRatio - 4, barHeight - 4), block);
		}
	}

	public void DecreaseHealth(float deltaHealth, Stats playerStats)
	{
		Debug.Log("Taking Damage ... health: "+this.stats.health);
		stats.health -= deltaHealth;
		if(stats.health <= 0)
		{
			playerStats.XP += this.stats.maxHealth;
			Destroy (this.gameObject);
		}
	}

	public void AttackPlayer(Stats playerStats)
	{
		playerStats.AdjustHealth(-stats.strength);
	}
}
